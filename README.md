# Painter Project
Implemented and ready for use

### Prerequisites

Please make sure you have latest SBT installed on your environment!


### Implementation Details

1) Solution not using any frameworks apart of that was used for testing
2) I prefer scala because it can reduce boilerplate code

### Running

When you have your SBT installed please in terminal open Project folder
and execute next command start command line:

```
sbt run

```

You should see following:

```
[Please input command] >> 

```

Now you should be able to execute commands, while list examples of possible commands is:

```
[Please input command] >> C 10 10
[Please input command] >> L 0 0 0 7
[Please input command] >> R 0 0 4 4
[Please input command] >> B 0 0 x
[Please input command] >> REVERT 1
[Please input command] >> Q

```
##### Note
- You could change the parameters in command, but cant change the format, other way you will have "Unknown command" warning.
- Q will quit program
- REVERT <N> will revert N of your commands
- Rest of the commands should works as expected

### Testing

In order to run unit testing please execute next command:

```
sbt test

```