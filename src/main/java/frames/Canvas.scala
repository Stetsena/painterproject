package frames

/**
  * Created by stetsa on 5/27/2017.
  */
object Canvas {
  def apply(width: Int, lenght: Int): Canvas = new Canvas(width, lenght)
}

class Canvas(val width: Int, val height: Int) {
  var pointList: List[List[Char]] = init()

  private def init() = {
    List.fill(height)((" " * width).toList)
  }


  override def toString() : String = {
    var outputSring = "-" * (width+2) + "\n"
    for (id <- pointList.size-1 to 0 by -1) outputSring = outputSring + "|" + pointList(id).mkString + "|"+ "\n"
    outputSring += "-" * (width+2) + "\n"
    outputSring
  }

}
