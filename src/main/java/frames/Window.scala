package frames

import commands.History
import traits.Undoable

/**
  * Created by stetsa on 5/27/2017.
  */
object Window {
  def apply(): Window = new Window()
}

class Window {
  var workingCanvas: Canvas = Canvas(32,12)
  val history: History = History()

  def applyCommand(command : ((Canvas) => Either[Canvas, String]) with Undoable) : String = {
   val outputMessage = command.apply(workingCanvas).fold(
     canvas => {
       workingCanvas = canvas
       history.add(command)
       canvas.toString
     },
     error =>  error)
     outputMessage
  }

  def revertLast(number : Int) : String = {
   for(a <- 1 to number) workingCanvas = history.undo(workingCanvas)
    workingCanvas.toString()
  }

}
