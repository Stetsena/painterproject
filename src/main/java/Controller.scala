/**
  * Created by stetsa on 5/27/2017.
  */
import com.sun.org.apache.xerces.internal.impl.xpath.regex.RegularExpression
import commands.{BucketFill, DrawLine, DrawRectangle, NewCanvas}
import frames.Window
import sun.awt.OSInfo.WindowsVersion
import traits.RegularExpressions

import scala.io.StdIn.readLine

object Controller extends RegularExpressions {

  val Response = "Response -> \n"

  def main(args: Array[String]): Unit = {

    var runningState = true
    val window: Window = Window()

    while (runningState) {
      val line: String = readLine("[Please input command] >> ")
      if(!line.isEmpty) {
        try {
          line match {
            case newCanvas(width, length) => {
              println(Response + window.applyCommand(NewCanvas(width.toInt, length.toInt)))
            }
            case drawLine(x1, y1, x2, y2) => {
              println(Response + window.applyCommand(DrawLine((x1.toInt,y1.toInt), (x2.toInt,y2.toInt), defaultSymbol)))
            }
            case drawRectangle(x1, y1, x2, y2) => {
              println(Response + window.applyCommand(DrawRectangle((x1.toInt,y1.toInt), (x2.toInt,y2.toInt), defaultSymbol)))
            }
            case bucketFill(x, y , symbol) => {
              println(Response + window.applyCommand(BucketFill((x.toInt, y.toInt), symbol.toCharArray.head)))
            }
            case revertLastCommand(x) => {
              println(Response + window.revertLast(x.toInt))
            }
            case "Q" => {
              runningState = false
            }
            case _ => println("Unknown Command")
          }
        }catch {
          case ex: Throwable => println("Response -> "+ex.getMessage)
        }
      }
    }

  }
}
