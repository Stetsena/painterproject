package traits

/**
  * Created by stetsa on 5/28/2017.
  */
trait RegularExpressions {
  val newCanvas = "C[ ]{1,9}([0-9]{1,3})[ ]{1,9}([0-9]{1,3})".r
  val drawLine = "L[ ]{1,9}([0-9]{1,3})[ ]{1,9}([0-9]{1,3})[ ]{1,9}([0-9]{1,3})[ ]{1,9}([0-9]{1,3})".r
  val drawRectangle = "R[ ]{1,9}([0-9]{1,3})[ ]{1,9}([0-9]{1,3})[ ]{1,9}([0-9]{1,3})[ ]{1,9}([0-9]{1,3})".r
  val bucketFill = "B[ ]{1,9}([0-9]{1,3})[ ]{1,9}([0-9]{1,3})[ ]{1,9}([0-9a-zA-Z]{1})".r
  val revertLastCommand = "REVERT[ ]{1,9}([0-9]{1,2})".r
  val defaultSymbol = 'x'
}
