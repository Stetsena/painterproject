package traits

import frames.Canvas

/**
  * Created by stetsa on 5/27/2017.
  */
trait Undoable {
  def undo(canvas: Canvas): Canvas
}