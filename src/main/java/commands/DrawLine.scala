package commands

import frames.Canvas
import traits.Undoable

/**
  * Created by stetsa on 5/27/2017.
  */
object DrawLine {
  def apply(point1: (Int, Int), point2: (Int, Int), symbol: Char): DrawLine = new DrawLine(point1, point2, symbol)
}


class DrawLine(point1 : (Int, Int), point2 : (Int, Int), symbol :Char) extends ((Canvas) => Either[Canvas, String]) with Undoable {

  var originalValue : List[Option[(Int,Int, Char)]] = List()

  def apply(canvas: Canvas): Either[Canvas, String] = {
    if (point1._1 != point2._1 && point1._2 != point2._2) {
      return Right("Diagonal lines is not supported for now")
    }
    originalValue = getLineCoordinates(canvas)(point1._1, point1._2, point2._1, point2._2)
    originalValue.flatten.foreach(coordinate => {
      canvas.pointList = canvas.pointList.patch(coordinate._1,
        List(canvas.pointList(coordinate._1).patch(coordinate._2, List(symbol), 1)),
        1)
    })
    Left(canvas)
  }

  def undo(canvas: Canvas) : Canvas = {
    originalValue.flatten.foreach(param => {
      canvas.pointList = canvas.pointList.patch(param._1,
        List(canvas.pointList(param._1).patch(param._2, List(param._3), 1)),
        1)
    })
    canvas
  }

  private def getLineCoordinates(canvas: Canvas)(x1: Int, y1: Int, x2: Int, y2: Int) : List[Option[(Int,Int, Char)]] ={
    var startX = Math.min(x1, x2)
    var stopX = Math.max(x1, x2)
    var startY = Math.min(y1, y2)
    var stopY = Math.max(y1, y2)
    var pointsToPaint : List[Option[(Int,Int, Char)]] = List()
    if (startY <= canvas.height && stopY >= 0 && startX <= canvas.width && stopX >= 0) {
      if(startX == stopX) {
        var valueToStart : Int = Math.max(startY, 0)
        var valueToStop : Int = Math.min(stopY, canvas.height-1)

        pointsToPaint = (valueToStart to valueToStop).map(point => {
          if(!canvas.pointList(point)(startX).equals(symbol))
          Some((point , startX, canvas.pointList(point)(startX)))
          else None
        }).toList
      }else {
        var valueToStart : Int = Math.max(startX, 0)
        var valueToStop : Int = Math.min(stopX, canvas.width-1)
        pointsToPaint = (valueToStart to valueToStop).map(point => {
          if(!canvas.pointList(startY)(point).equals(symbol))
          Some(( startY, point, canvas.pointList(startY)(point)))
          else None
        }).toList
      }
    }
    pointsToPaint
  }


}
