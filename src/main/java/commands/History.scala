package commands

import frames.Canvas
import traits.Undoable


/**
  * Created by stetsa on 5/27/2017.
  */
object History {
  def apply(): History = new History()
}

class History {

  var commands: List[((Canvas) => Either[Canvas, String]) with Undoable] = List()

  def add(command : ((Canvas) => Either[Canvas, String]) with Undoable) ={
    commands = commands :+ command
  }

  def undo(canvas: Canvas): Canvas = {
    val revertedCanvas = if(commands.size >0) commands.last.undo(canvas) else canvas
    commands = commands.dropRight(1)
    revertedCanvas
  }

}

