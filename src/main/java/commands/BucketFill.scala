package commands

import frames.Canvas
import traits.Undoable

import scala.annotation.tailrec

/**
  * Created by stetsa on 5/28/2017.
  */
object BucketFill {
  def apply(point: (Int, Int), symbol: Char): BucketFill = new BucketFill(point, symbol)
}

class BucketFill(point : (Int, Int), symbol :Char) extends ((Canvas) => Either[Canvas, String]) with Undoable {
  var originalValue = ' '

  override def apply(canvas: Canvas): Either[Canvas, String] = {
    if(point._1 < 0 || point._1 > canvas.width
      || point._2 < 0 || point._2 > canvas.height) {
      Right("Point doesn't belong to canvas, nothing to fill")
    }
    originalValue = canvas.pointList(point._2)(point._1).toString.head
    fill(Set(point), canvas, originalValue, symbol)
    Left(canvas)
  }

  @tailrec
  final def fill(listOfPoints : Set[(Int, Int)], canvas: Canvas, originalSymbol: Char, symbolToFill: Char) : Canvas = {
    if(listOfPoints.isEmpty) return canvas
    var nextRound : Set[(Int, Int)] = Set()
    listOfPoints.foreach( point => {
      canvas.pointList = canvas.pointList.patch(point._2,
        List(canvas.pointList(point._2).patch(point._1, List(symbolToFill), 1)),
        1)
      nextRound = nextRound ++ getFillablePointNeigbours(point, canvas, originalSymbol)
    })
    fill(nextRound, canvas, originalSymbol, symbolToFill)
  }

  def getFillablePointNeigbours(point : (Int, Int), canvas: Canvas, symbol: Char) : Set[(Int, Int)] = {
    var listOfPoints : Set[(Int, Int)] = Set()
    val check = checkPoint(canvas,symbol)_
    listOfPoints = listOfPoints ++ check((point._1+1, point._2))
    listOfPoints = listOfPoints ++ check((point._1-1, point._2))
    listOfPoints = listOfPoints ++ check((point._1, point._2+1))
    listOfPoints = listOfPoints ++ check((point._1, point._2-1))
    listOfPoints = listOfPoints ++ check((point._1+1, point._2-1))
    listOfPoints = listOfPoints ++ check((point._1-1, point._2+1))
    listOfPoints = listOfPoints ++ check((point._1+1, point._2+1))
    listOfPoints = listOfPoints ++ check((point._1-1, point._2-1))
    listOfPoints
  }

  def checkPoint(canvas: Canvas, symbol: Char)(point : (Int, Int)) : Option[(Int, Int)] = {
    if(point._1 >= 0 && point._1 < canvas.height
      && point._2 >= 0 && point._2 < canvas.width
      && canvas.pointList(point._2)(point._1).equals(symbol)){
      Some(point)
    } else {
      None
    }
  }

  override def undo(canvas: Canvas): Canvas = {
    fill(Set(point), canvas, symbol, originalValue)
    canvas
  }
}
