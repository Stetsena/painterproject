package commands

import frames.Canvas
import traits.Undoable
/**
  * Created by stetsa on 5/27/2017.
  */

object NewCanvas {
  def apply(widths: Int, lenghts: Int): NewCanvas = new NewCanvas(widths, lenghts)
}

class NewCanvas(widths: Int, lenghts: Int) extends ((Canvas) => Either[Canvas, String]) with Undoable {
  var oldCanvas: Canvas = Canvas(0, 0)

  def apply(canvas: Canvas): Either[Canvas, String]= {
    oldCanvas = canvas
    Left(Canvas(widths, lenghts))
  }

  def undo(canvas: Canvas) = oldCanvas
}


