package commands

import frames.Canvas
import traits.Undoable

/**
  * Created by stetsa on 5/27/2017.
  */
object DrawRectangle {
  def apply(leftBotom: (Int, Int), rightTop: (Int, Int), symbol: Char): DrawRectangle = new DrawRectangle(leftBotom, rightTop, symbol)
}

class DrawRectangle (leftBotom : (Int, Int), rightTop : (Int, Int), symbol :Char) extends ((Canvas) => Either[Canvas, String]) with Undoable {

  var originalValue : List[((Canvas) => Either[Canvas, String]) with Undoable] = List()
  val leftTop = (leftBotom._1, rightTop._2)
  val rightBottom = (rightTop._1, leftBotom._2)

  override def apply(canvas: Canvas): Either[Canvas, String] = {
    save(canvas, DrawLine(leftBotom, leftTop, symbol)).fold(left => {
      save(left, DrawLine(leftTop, rightTop, symbol)).fold(leftRes => {
        save(leftRes, DrawLine(rightTop, rightBottom, symbol)).fold(leftResult => {
          save(leftResult, DrawLine(leftBotom, rightBottom, symbol))
        }, rightResult => {Right(rightResult)})
      }, rightRes => {Right(rightRes)})
    }, right => {Right(right)})
  }

  def save(canvas: Canvas , command : ((Canvas) => Either[Canvas, String]) with Undoable) : Either[Canvas, String] = {
    val toReturn = command.apply(canvas)
    originalValue = originalValue :+ command
    toReturn
  }

  override def undo(canvas: Canvas): Canvas = {
    var canvasToReturn: Canvas = canvas
    originalValue.foreach( command => {
      canvasToReturn = command.undo(canvasToReturn)
    })
    canvasToReturn
  }
}
