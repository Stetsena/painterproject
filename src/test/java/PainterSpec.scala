/**
  * Created by stetsa on 5/28/2017.
  */

import commands.{DrawLine, NewCanvas, DrawRectangle, BucketFill}
import frames.Window
import org.scalatest.{BeforeAndAfterAll, FlatSpec, Matchers}

class PainterSpec extends FlatSpec with Matchers with BeforeAndAfterAll {
  val mainWindow: Window = Window()

  override def beforeAll: Unit = {
    mainWindow.applyCommand(NewCanvas(34, 12))
  }

  "Window DrawLine command" should "draw the horizontal line" in {
    mainWindow.applyCommand(NewCanvas(34, 12))
    mainWindow.applyCommand(DrawLine((0, 0), (8, 0), 'x')) should equal(
        "------------------------------------\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|xxxxxxxxx                         |\n" +
        "------------------------------------\n"
    )
  }

  "Window DrawLine command" should "draw the vertical line" in {
    mainWindow.applyCommand(NewCanvas(34, 12))
    mainWindow.applyCommand(DrawLine((0, 0), (0, 6), 'x')) should equal(
        "------------------------------------\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|x                                 |\n" +
        "|x                                 |\n" +
        "|x                                 |\n" +
        "|x                                 |\n" +
        "|x                                 |\n" +
        "|x                                 |\n" +
        "|x                                 |\n" +
        "------------------------------------\n"
    )
  }

  "Window DrawLine command" should "draw return warning in case of diagonal line" in {
    mainWindow.applyCommand(NewCanvas(34, 12))
    mainWindow.applyCommand(DrawLine((0, 0), (6, 2), 'x')) should equal("Diagonal lines is not supported for now")
  }

  "Window DrawLine command" should "draw the vertical and horizontal line with different colors" in {
    mainWindow.applyCommand(NewCanvas(34, 12))
    mainWindow.applyCommand(DrawLine((0, 0), (6, 0), 'x'))
    mainWindow.applyCommand(DrawLine((0, 0), (0, 8), 't')) should equal(
       "------------------------------------\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|t                                 |\n" +
        "|t                                 |\n" +
        "|t                                 |\n" +
        "|t                                 |\n" +
        "|t                                 |\n" +
        "|t                                 |\n" +
        "|t                                 |\n" +
        "|t                                 |\n" +
        "|txxxxxx                           |\n" +
        "------------------------------------\n"
    )
  }

  "Window DrawRactangle command" should "draw ractangle" in {
    mainWindow.applyCommand(NewCanvas(34, 12))
    mainWindow.applyCommand(DrawRectangle((0, 0), (5, 3), 'x')) should equal(
        "------------------------------------\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|xxxxxx                            |\n" +
        "|x    x                            |\n" +
        "|x    x                            |\n" +
        "|xxxxxx                            |\n" +
        "------------------------------------\n"
    )
  }

  "Window DrawRactangle command" should "draw ractangle in ractangle with different colors" in {
    mainWindow.applyCommand(NewCanvas(34, 12))
    mainWindow.applyCommand(DrawRectangle((0, 0), (5, 3), 'x'))
    mainWindow.applyCommand(DrawRectangle((0, 0), (5, 5), 't')) should equal(
      "------------------------------------\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|tttttt                            |\n" +
        "|t    t                            |\n" +
        "|txxxxt                            |\n" +
        "|t    t                            |\n" +
        "|t    t                            |\n" +
        "|tttttt                            |\n" +
        "------------------------------------\n"
    )
  }

  "Window Revert command" should "be able to revert merged changes" in {
    mainWindow.applyCommand(NewCanvas(34, 12))
    mainWindow.applyCommand(DrawRectangle((0, 0), (5, 3), 'x'))
    mainWindow.applyCommand(DrawRectangle((0, 0), (5, 5), 't'))
    mainWindow.revertLast(1) should equal(
        "------------------------------------\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|xxxxxx                            |\n" +
        "|x    x                            |\n" +
        "|x    x                            |\n" +
        "|xxxxxx                            |\n" +
        "------------------------------------\n"
    )
  }

  "Window BucketFill command" should "fill area with choosen symbol" in {
    mainWindow.applyCommand(NewCanvas(34, 12))
    mainWindow.applyCommand(DrawRectangle((0, 0), (5, 3), 'x'))
    mainWindow.applyCommand(DrawRectangle((0, 0), (5, 5), 'x'))
    mainWindow.applyCommand(BucketFill((1, 1), 'o')) should equal(
      "------------------------------------\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|xxxxxx                            |\n" +
        "|x    x                            |\n" +
        "|xxxxxx                            |\n" +
        "|xoooox                            |\n" +
        "|xoooox                            |\n" +
        "|xxxxxx                            |\n" +
        "------------------------------------\n"
    )
  }

  "Window multiple commands" should "be able to draw complex picture with choosen symbol" in {
    mainWindow.applyCommand(NewCanvas(34, 12))
    mainWindow.applyCommand(DrawRectangle((0, 0), (5, 3), 'x'))
    mainWindow.applyCommand(DrawRectangle((0, 0), (5, 5), 'x'))
    mainWindow.applyCommand(DrawLine((0, 7), (45, 7), 'x'))
    mainWindow.applyCommand(DrawLine((7, 0), (7, 56), 'x')) should equal(
      "------------------------------------\n" +
        "|       x                          |\n" +
        "|       x                          |\n" +
        "|       x                          |\n" +
        "|       x                          |\n" +
        "|xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx|\n" +
        "|       x                          |\n" +
        "|xxxxxx x                          |\n" +
        "|x    x x                          |\n" +
        "|xxxxxx x                          |\n" +
        "|x    x x                          |\n" +
        "|x    x x                          |\n" +
        "|xxxxxx x                          |\n" +
        "------------------------------------\n"
    )
  }

  "Window BucketFill command" should "fill complex area with choosen symbol" in {
    mainWindow.applyCommand(NewCanvas(34, 12))
    mainWindow.applyCommand(DrawRectangle((0, 0), (5, 3), 'x'))
    mainWindow.applyCommand(DrawRectangle((0, 0), (5, 5), 'x'))
    mainWindow.applyCommand(DrawLine((0, 7), (45, 7), 'x'))
    mainWindow.applyCommand(DrawLine((7, 0), (7, 56), 'x'))
    mainWindow.applyCommand(BucketFill((1, 6), 'o')) should equal(
        "------------------------------------\n" +
        "|       x                          |\n" +
        "|       x                          |\n" +
        "|       x                          |\n" +
        "|       x                          |\n" +
        "|xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx|\n" +
        "|ooooooox                          |\n" +
        "|xxxxxxox                          |\n" +
        "|x    xox                          |\n" +
        "|xxxxxxox                          |\n" +
        "|x    xox                          |\n" +
        "|x    xox                          |\n" +
        "|xxxxxxox                          |\n" +
        "------------------------------------\n"
    )
  }


  "Window Revert command" should "revert BucketFill command" in {
    mainWindow.applyCommand(NewCanvas(34, 12))
    mainWindow.applyCommand(DrawRectangle((0, 0), (5, 3), 'x'))
    mainWindow.applyCommand(DrawRectangle((0, 0), (5, 5), 'x'))
    mainWindow.applyCommand(BucketFill((1, 1), 'o'))
    mainWindow.revertLast(1) should equal(
      "------------------------------------\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|xxxxxx                            |\n" +
        "|x    x                            |\n" +
        "|xxxxxx                            |\n" +
        "|x    x                            |\n" +
        "|x    x                            |\n" +
        "|xxxxxx                            |\n" +
        "------------------------------------\n"
    )
  }

  "Window Revert command" should "revert NewCanvas command" in {
    mainWindow.applyCommand(NewCanvas(34, 12))
    mainWindow.applyCommand(DrawRectangle((0, 0), (5, 3), 'x'))
    mainWindow.applyCommand(DrawRectangle((0, 0), (5, 5), 'x'))
    mainWindow.applyCommand(NewCanvas(2, 2))
    mainWindow.revertLast(1) should equal(
        "------------------------------------\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|                                  |\n" +
        "|xxxxxx                            |\n" +
        "|x    x                            |\n" +
        "|xxxxxx                            |\n" +
        "|x    x                            |\n" +
        "|x    x                            |\n" +
        "|xxxxxx                            |\n" +
        "------------------------------------\n"
    )
  }


}
